
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Kohana Sample</a>
    </div>

  </div><!-- /.container-fluid -->
</nav>

<div class="container" style="margin-top: 100px">

    
     
      <?php foreach($posts as $post) { ?>
       <div class="jumbotron">
	   <h1><?php echo $post->title; ?> </h1>
	   <p>
	       <?php echo $post->description ?>
	   </p>
	   <p><a class="btn btn-primary btn-lg" role="button">Learn more</a></p>
    </div>     
      <?php } ?>

</div>